## Prepare release

- [ ] Create a release branch from the right revision of the `master` branch / release tag following the naming convention `release/MAJOR.MINOR.PATCH`

### Preparation is finished when:

- [ ] [pyproject.toml](pyproject.toml) contains the right release number
- [ ] [CHANGELOG.md](CHANGELOG.md) is up-to-date
- [ ] Sphinx generated documentation is up-to-date
- [ ] The build pipeline ran properly and indicated no problems
- [ ] The release package has been checked manually

## Create release

- [ ] Tag the release branch using the naming template `MAJOR.MINOR.PATCH`
- [ ] Merge the release into the `master` including the tags
- [ ] Add release notes to the GitLab tag (link to the changelog and attach release build artifacts)
- [ ] Delete the release branch

## Post release actions

- [ ] Set new development version
- [ ] Announce the new release


/label ~"type::release"
