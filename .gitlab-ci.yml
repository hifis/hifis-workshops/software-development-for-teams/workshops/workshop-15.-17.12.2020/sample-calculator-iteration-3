# SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
# SPDX-License-Identifier: MIT


variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
  paths:
    - .cache/pip
    - .venv
  key: "${CI_JOB_NAME}"

stages:
  - test
  - audit
  - package
  - deploy

.install-deps-template: &install-deps
  before_script:
    - pip install poetry
    - poetry --version
    - poetry config virtualenvs.in-project true
    - poetry install -vv

test:
  <<: *install-deps
  image: python:3.6
  stage: test
  script:
    - make test
  tags:
    - docker
  except:
    - tags
  artifacts:
    reports:
      junit: build/tests.xml

.audit-template: &audit
  <<: *install-deps
  image: python:3.6
  stage: audit
  tags:
    - docker
  except:
    - tags

check-code:
  <<: *audit
  script:
    - make check-code
  artifacts:
    when: on_failure
    expose_as: "Linter Report"
    paths: ["build/flake8.txt"]

check-coverage:
  <<: *audit
  script:
    - make check-coverage
  coverage: '/^TOTAL.+?(\d+\%)$/'
  artifacts:
    expose_as: "Coverage Report"
    paths: ["build/htmlcov/"]

check-license-metadata:
  <<: *audit
  script:
    - make check-license-metadata

check-security:
  <<: *audit
  script:
    - make check-security

snapshot:
  <<: *install-deps
  stage: package
  image: python:3.6
  script:
    - 'currentVersion=$(poetry version --no-ansi)'
    - 'currentVersion="${currentVersion: 18}-alpha+${CI_COMMIT_SHORT_SHA}"'
    - poetry version $currentVersion
    - make package
  tags:
    - docker
  only:
    - master
  artifacts:
    paths:
      - build
      - dist
      - docs/html

release:
  <<: *install-deps
  stage: package
  image: python:3.6
  script:
    - make package
  tags:
    - docker
  only:
    - /^release/
  artifacts:
    expose_as: "Release Artifacts"
    paths: ["dist/", "docs/html/"]

pages:
  stage: deploy
  script:
    - mv docs/html public
  tags:
    - docker
  only:
    - master
  artifacts:
    paths:
    - public
